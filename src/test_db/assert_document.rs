// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use crate::{test_db::test_error::TestError, test_error};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

/// Assert a option of a document `is_some`
///
/// ---
/// Example Usage:
/// ```
///
/// let document_option: Option<D> = ...;
///
/// assert_document_is_some(&document_option)?;
/// ```
pub fn assert_document_is_some<D>(option: &Option<D>) -> Result<(), Box<dyn std::error::Error>>
where
    D: Serialize + for<'a> Deserialize<'a> + Send + Sync + Unpin + Debug,
{
    if option.is_none() {
        test_error!("Expected document option {:?} to be some, got None", option);
    }

    Ok(())
}

/// Assert a option of a document `is_none`
///
/// ---
/// Example Usage:
/// ```
///
/// let document_option: Option<D> = ...;
///
/// assert_document_is_none(&document_option)?;
/// ```
pub fn assert_document_is_none<D>(option: &Option<D>) -> Result<(), Box<dyn std::error::Error>>
where
    D: Serialize + for<'a> Deserialize<'a> + Send + Sync + Unpin + Debug,
{
    if option.is_some() {
        test_error!("Expected document option to be None, got {:?}", option);
    }

    Ok(())
}
