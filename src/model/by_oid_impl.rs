// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use super::Model;
use crate::{
    core::{delete::*, find::*, replace::*, update::*},
    error::Error,
};
use mongodb::{
    bson::{doc, oid::ObjectId, Document},
    options::*,
    results::{DeleteResult, UpdateResult},
    ClientSession,
};
use serde::{Deserialize, Serialize};

impl<D> Model<D>
where
    D: Serialize + for<'a> Deserialize<'a> + Send + Sync + Unpin,
{
    /// Executes a `findOne` query on `self.collection` by the `_id` field.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.findOne/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let oid: ObjectId = ...;
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let document_option: Option<D> = model.find_one_by_oid(
    ///     &oid,
    ///     None::<FindOneOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn find_one_by_oid(
        &self,
        oid: &ObjectId,
        options: Option<FindOneOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<Option<D>, Error> {
        find_one(&self.collection, doc! { "_id": oid }, options, session).await
    }

    /// Executes a `deleteOne` query on `self.collection` by the `_id` field.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.deleteOne/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let oid: ObjectId = ...;
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let deletion_result: DeleteResult = model.delete_one_by_oid(
    ///     &oid,
    ///     None::<DeleteOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn delete_one_by_oid(
        &self,
        oid: &ObjectId,
        options: Option<DeleteOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<DeleteResult, Error> {
        delete_one(&self.collection, doc! { "_id": oid }, options, session).await
    }

    /// Executes a `updateOne` query on `self.collection` by the `_id` field.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.updateOne/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let oid: ObjectId = ...;
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let update_result: UpdateResult = model.update_one_by_oid(
    ///     &oid,
    ///     doc! { ... },
    ///     None::<UpdateOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn update_one_by_oid(
        &self,
        oid: &ObjectId,
        update: Document,
        options: Option<UpdateOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<UpdateResult, Error> {
        update_one(
            &self.collection,
            doc! { "_id": oid },
            update,
            options,
            session,
        )
        .await
    }

    /// Executes a `findOneAndUpdate` query on `self.collection`
    /// by the `_id` field, returns the original document by default.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.findOneAndUpdate/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let oid: ObjectId = ...;
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let updated_document: Option<D> = model.find_one_and_update_by_oid(
    ///     &oid,
    ///     doc! { ... },
    ///     None::<FindOneAndUpdateOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn find_one_and_update_by_oid(
        &self,
        oid: &ObjectId,
        update: Document,
        options: Option<FindOneAndUpdateOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<Option<D>, Error> {
        find_one_and_update(
            &self.collection,
            doc! { "_id": oid },
            update,
            options,
            session,
        )
        .await
    }

    /// Executes a `findOneAndReplace` query on `self.collection`
    /// by the `_id` field, returns the original document by default.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.findOneAndReplace/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let oid: ObjectId = ...;
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let replacement: D = ...;
    ///
    /// let original_document: Option<D> = model.find_one_and_replace_by_oid(
    ///     &oid,
    ///     replacement,
    ///     None::<FindOneAndReplaceOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn find_one_and_replace_by_oid(
        &self,
        oid: &ObjectId,
        replacement: D,
        options: Option<FindOneAndReplaceOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<Option<D>, Error> {
        find_one_and_replace(
            &self.collection,
            doc! { "_id": oid },
            replacement,
            options,
            session,
        )
        .await
    }

    /// Returns if a document exists by a oid in `self.collection`.
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let oid: ObjectId = ...;
    ///
    /// if model.exists_by_oid(
    ///     &oid,
    ///     None::<FindOneOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await? {
    ///     ...
    /// }
    /// ```
    pub async fn exists_by_oid(
        &self,
        oid: &ObjectId,
        options: Option<FindOneOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<bool, Error> {
        Ok(self.find_one_by_oid(oid, options, session).await?.is_some())
    }
}
