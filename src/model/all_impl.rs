// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use super::Model;
use crate::{core::find::FindManyCursor, error::Error};
use mongodb::{
    bson::{doc, Document},
    options::*,
    results::{DeleteResult, UpdateResult},
    ClientSession,
};
use serde::{Deserialize, Serialize};

impl<D> Model<D>
where
    D: Serialize + for<'a> Deserialize<'a> + Send + Sync + Unpin,
{
    /// Returns all documents in `self.collection`.
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let mut cursor = model.find_all(
    ///     None::<FindOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    ///
    /// while let Some(document: D) = cursor.next().await? {
    ///     ...
    /// }
    /// ```
    pub async fn find_all(
        &self,
        options: Option<FindOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<FindManyCursor<D>, Error> {
        self.find_many(doc! {}, options, session).await
    }

    /// Updates all documents in `self.collection`.
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let update_result: UpdateResult = model.update_all(
    ///     doc! { ... },
    ///     None::<UpdateOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn update_all(
        &self,
        update: Document,
        options: Option<UpdateOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<UpdateResult, Error> {
        self.update_many(doc! {}, update, options, session).await
    }

    /// Deletes all documents in `self.collection`.
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let mut model: Model<D> = ...;
    ///
    /// let delete_result: DeleteResult = model.delete_all(
    ///     None::<DeleteOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn delete_all(
        &self,
        options: Option<DeleteOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<DeleteResult, Error> {
        self.delete_many(doc! {}, options, session).await
    }
}
