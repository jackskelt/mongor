// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use super::Model;
use crate::{
    core::{
        aggregate::AggregateCursor, delete::*, find::*, index::*, insert::*, replace::*, update::*,
    },
    error::Error,
};
use mongodb::{
    bson::{oid::ObjectId, Document},
    options::*,
    results::{DeleteResult, UpdateResult},
    ClientSession, IndexModel,
};
use serde::{Deserialize, Serialize};

impl<D> Model<D>
where
    D: Serialize + for<'a> Deserialize<'a> + Send + Sync + Unpin,
{
    /// Executes a `findOne` query on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.findOne/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let document: Option<D> = model.find_one(
    ///     doc! { ... },
    ///     None::<FindOneOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn find_one(
        &self,
        filter: Document,
        options: Option<FindOneOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<Option<D>, Error> {
        find_one(&self.collection, filter, options, session).await
    }

    /// Executes a `find` query on `self.collection` and returns a cursor to iterate over.
    ///
    /// See: `core::find::FindManyCursor`
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.find/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let mut cursor: FindManyCursor<D> = model.find_many(
    ///     doc! { ... },
    ///     None::<FindOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    ///
    /// // Iteration
    /// while let Some(document: D) = cursor.next_with_session(&mut session).await? {
    ///     ...
    /// }
    ///
    /// // Or, to get all at once
    /// let documents: Vec<D> = cursor.all_with_session().await?;
    /// ```
    pub async fn find_many(
        &self,
        filter: Document,
        options: Option<FindOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<FindManyCursor<D>, Error> {
        Ok(FindManyCursor::from(&self.collection, filter, options, session).await?)
    }

    /// Executes a `aggregate` query on `self.collection` and returns a cursor to iterate over.
    ///
    /// See: `core::aggregate::AggregateCursor`
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.aggregate/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let pipeline: Vec<Document> = vec![
    ///     doc! {
    ///         "$match": {
    ///             ...
    ///         }
    ///     },
    ///     ...
    /// ];
    ///
    /// let model: Model<D> = ...;
    ///
    /// let mut cursor: AggregateCursor<D> = model.aggregate(
    ///     pipeline,
    ///     None::<AggregateOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    ///
    /// while let Some(document: D) = cursor.next_with_session(&mut session).await? {
    ///     ...
    /// }
    /// ```
    pub async fn aggregate(
        &self,
        pipeline: Vec<Document>,
        options: Option<AggregateOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<AggregateCursor<D>, Error> {
        Ok(AggregateCursor::from(&self.collection, pipeline, options, session).await?)
    }

    /// Executes a `deleteOne` query on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.deleteOne/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let deletion_result: DeleteResult = model.delete_one(
    ///     doc! { ... },
    ///     None::<DeleteOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn delete_one(
        &self,
        query: Document,
        options: Option<DeleteOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<DeleteResult, Error> {
        delete_one(&self.collection, query, options, session).await
    }

    /// Executes a `deleteMany` query on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.deleteMany/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let deletion_result: DeleteResult = model.delete_many(
    ///     doc! { ... },
    ///     None::<DeleteOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn delete_many(
        &self,
        query: Document,
        options: Option<DeleteOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<DeleteResult, Error> {
        delete_many(&self.collection, query, options, session).await
    }

    /// Executes a `insertOne` query on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.insertOne/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let document: D = ...;
    ///
    /// let inserted_oid: ObjectId = model.insert_one(
    ///     document,
    ///     None::<InsertOneOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn insert_one(
        &self,
        document: D,
        options: Option<InsertOneOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<ObjectId, Error> {
        insert_one(&self.collection, document, options, session).await
    }

    /// Executes a `insertMany` query on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.insertMany/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let documents: Vec<D> = vec![...];
    ///
    /// let inserted_oids: Vec<ObjectId> = model.insert_many(
    ///     documents,
    ///     None::<InsertManyOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn insert_many(
        &self,
        documents: Vec<D>,
        options: Option<InsertManyOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<Vec<ObjectId>, Error> {
        insert_many(&self.collection, documents, options, session).await
    }

    /// Executes a `updateOne` query on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.updateOne/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let update_result: UpdateResult = model.update_one(
    ///     doc! { ... },
    ///     doc! { ... },
    ///     None::<UpdateOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn update_one(
        &self,
        query: Document,
        update: Document,
        options: Option<UpdateOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<UpdateResult, Error> {
        update_one(&self.collection, query, update, options, session).await
    }

    /// Executes a `updateMany` query on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.updateMany/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let update_result: UpdateResult = model.update_many(
    ///     doc! { ... },
    ///     doc! { ... },
    ///     None::<UpdateOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn update_many(
        &self,
        query: Document,
        update: Document,
        options: Option<UpdateOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<UpdateResult, Error> {
        update_many(&self.collection, query, update, options, session).await
    }

    /// Executes a `findOneAndUpdate` query on `self.collection`,
    /// returns the original document by default.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.findOneAndUpdate/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let original_document: D = model.find_one_and_update(
    ///     doc! { ... },
    ///     doc! { ... },
    ///     None::<FindOneAndUpdateOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn find_one_and_update(
        &self,
        query: Document,
        update: Document,
        options: Option<FindOneAndUpdateOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<Option<D>, Error> {
        find_one_and_update(&self.collection, query, update, options, session).await
    }

    /// Executes a `findOneAndReplace` query on `self.collection`,
    /// returns the original document by default.
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.findOneAndReplace/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let replacement: D = ...;
    ///
    /// let original_document: D = model.find_one_and_replace(
    ///     doc! { ... },
    ///     replacement,
    ///     None::<FindOneAndReplaceOptions>,
    ///     None::<&mut ClientSession>,
    /// ).await?;
    /// ```
    pub async fn find_one_and_replace(
        &self,
        query: Document,
        replacement: D,
        options: Option<FindOneAndReplaceOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<Option<D>, Error> {
        find_one_and_replace(&self.collection, query, replacement, options, session).await
    }

    /// Drop an index by `name` on a `Collection`
    ///
    /// https://www.mongodb.com/docs/manual/reference/method/db.collection.dropIndex/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let collection: Collection<D> = ...;
    ///
    /// let index_model: IndexModel = build_blank_index(doc! { "field": 1 }, Some("index_1"));
    ///
    /// apply_index(&collection, index_model, None, None).await?;
    ///
    /// drop_index(&collection, "index_1", None, None).await?;
    /// ```
    pub async fn drop_index(
        &self,
        name: &str,
        options: Option<DropIndexOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<(), Error> {
        drop_index(&self.collection, name, options, session).await
    }

    /// Apply a `IndexModel` to a `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/indexes/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// let index_model: IndexModel = build_blank_index(doc! { "field": 1 }, None);
    ///
    /// model.apply_index(index_model, None, None).await?;
    /// ```
    pub async fn apply_index(
        &self,
        index_model: IndexModel,
        options: Option<CreateIndexOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<&Self, Error> {
        apply_index(&self.collection, index_model, options, session).await?;

        Ok(self)
    }

    /// Applies a Time-To-Live (TTL) `IndexModel` based on seconds
    /// on document field named `field_name` on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/core/index-ttl/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// model.apply_ttl_index(
    ///     86_400,
    ///     "created_at",
    ///     Some("ttl_index"),
    ///     None,
    ///     None,
    /// ).await?;
    /// ```
    pub async fn apply_ttl_index(
        &self,
        secs: u64,
        field_name: &str,
        name: Option<&str>,
        options: Option<CreateIndexOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<&Self, Error> {
        apply_index(
            &self.collection,
            build_ttl_index(secs, field_name, name),
            options,
            session,
        )
        .await?;

        Ok(self)
    }

    /// Applies a "blank" (No properties) `IndexModel` based on the provided
    /// fields on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/indexes/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// model.apply_blank_index(
    ///     doc! { "user": 1, "email": 1 },
    ///     Some("blank_index"),
    ///     None,
    ///     None,
    /// ).await?;
    /// ```
    pub async fn apply_blank_index(
        &self,
        fields: Document,
        name: Option<&str>,
        options: Option<CreateIndexOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<&Self, Error> {
        apply_index(
            &self.collection,
            build_blank_index(fields, name),
            options,
            session,
        )
        .await?;

        Ok(self)
    }

    /// Applies a unique `IndexModel` based on the the provided
    /// fields on `self.collection`.
    ///
    /// https://www.mongodb.com/docs/manual/core/index-unique/
    ///
    /// ---
    /// Example Usage:
    /// ```
    ///
    /// let model: Model<D> = ...;
    ///
    /// model.apply_unique_index(
    ///     doc! { "user": 1, "email": 1 },
    ///     Some("unique_index"),
    ///     None,
    ///     None,
    /// ).await?;
    /// ```
    pub async fn apply_unique_index(
        &self,
        fields: Document,
        name: Option<&str>,
        options: Option<CreateIndexOptions>,
        session: Option<&mut ClientSession>,
    ) -> Result<&Self, Error> {
        apply_index(
            &self.collection,
            build_unique_index(fields, name),
            options,
            session,
        )
        .await?;

        Ok(self)
    }
}
