// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use std::fmt;

/// Error enum to wrap various errors that can occur inside the crate.
///
/// SharedSessionLockFailure holds a stringified version of the poison
/// error.
#[derive(Debug, Clone)]
pub enum Error {
    Internal(String),
    IO(std::io::ErrorKind),
    Bson(mongodb::bson::de::Error),
    Mongo(mongodb::error::Error),
    TestError(String),
    SharedSessionLockFailure(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl std::error::Error for Error {}

impl Error {
    pub fn internal(message: &str) -> Self {
        Self::Internal(message.to_string())
    }
}
