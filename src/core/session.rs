// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use crate::error::Error;
use mongodb::{
    options::{SessionOptions, TransactionOptions},
    Client, ClientSession,
};
use std::sync::{Arc, Mutex};

/// Starts a new `ClientSession`.
///
/// https://www.mongodb.com/docs/manual/reference/method/Session/
///
/// ---
/// Example Usage:
/// ```
///
/// let client: Client = ...;
///
/// let mut session: ClientSession = start_session(
///     &client,
///     None::<SessionOptions>,
/// ).await?;
/// ```
pub async fn start_session(
    client: &Client,
    options: Option<SessionOptions>,
) -> Result<ClientSession, Error> {
    client
        .start_session(options)
        .await
        .map_err(|err| Error::Mongo(err))
}

/// Starts a new transaction on a `ClientSession`.
///
/// https://www.mongodb.com/docs/manual/reference/method/Session.startTransaction/
///
/// ---
/// Example Usage:
/// ```
///
/// let client: Client = ...;
///
/// let mut session: ClientSession = start_session(
///     &client,
///     None::<SessionOptions>,
/// ).await?;
///
/// start_transaction(&mut session, None).await?;
/// ```
pub async fn start_transaction(
    session: &mut ClientSession,
    options: Option<TransactionOptions>,
) -> Result<(), Error> {
    let options = options.unwrap_or_default();

    session
        .start_transaction(options)
        .await
        .map_err(|err| Error::Mongo(err))
}

/// Aborts the current transaction to a `ClientSession`.
///
/// https://www.mongodb.com/docs/manual/reference/method/Session.abortTransaction/
///
/// ---
/// Example Usage:
/// ```
///
/// let client: Client = ...;
///
/// let mut session: ClientSession = start_session(
///     &client,
///     None::<SessionOptions>,
/// ).await?;
///
/// start_transaction(&mut session, None).await?;
///
/// ...
///
/// abort_transaction(&mut session).await?;
/// ```
pub async fn abort_transaction(session: &mut ClientSession) -> Result<(), Error> {
    session
        .abort_transaction()
        .await
        .map_err(|err| Error::Mongo(err))
}

/// Aborts the current transaction to a `Arc<Mutex<ClientSession>>`.
///
/// https://www.mongodb.com/docs/manual/reference/method/Session.abortTransaction/
///
/// ---
/// Example Usage:
/// ```
///
/// let client: Client = ...;
///
/// let session: ClientSession = start_session(&client, None).await?;
/// let shared_session: Arc<Mutex<ClientSession>> = Arc::new(Mutex::new(session));
///
/// let mut session_lock = shared_session.lock().unwrap();
///
/// start_transaction(&mut session_lock, None).await?;
///
/// ...
///
/// abort_transaction_shared_session(&mut session_lock).await?;
/// ```
pub async fn abort_transaction_shared_session(
    shared_session: Arc<Mutex<ClientSession>>,
) -> Result<(), Error> {
    shared_session
        .lock()
        .map_err(|err| Error::SharedSessionLockFailure(err.to_string()))?
        .abort_transaction()
        .await
        .map_err(|err| Error::Mongo(err))
}

/// Commits the current transaction to a `ClientSession`.
///
/// https://www.mongodb.com/docs/manual/reference/method/Session.commitTransaction/
///
/// ---
/// Example Usage:
/// ```
///
/// let client: Client = ...;
///
/// let mut session: ClientSession = start_session(
///     &client,
///     None::<SessionOptions>,
/// ).await?;
///
/// start_transaction(&mut session, None).await?;
///
/// ...
///
/// commit_transaction(&mut session).await?;
/// ```
pub async fn commit_transaction(session: &mut ClientSession) -> Result<(), Error> {
    session
        .commit_transaction()
        .await
        .map_err(|err| Error::Mongo(err))
}
