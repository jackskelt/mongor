// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use crate::error::Error;
use mongodb::{
    bson::Document, options::DeleteOptions, results::DeleteResult, ClientSession, Collection,
};
use serde::{Deserialize, Serialize};

/// Executes a `deleteOne` query on `Collection`
///
/// https://www.mongodb.com/docs/manual/reference/method/db.collection.deleteOne/
///
/// ---
/// Example Usage:
/// ```
///
/// let collection: Collection<D> = ...;
///
/// let deletion_result: DeleteResult = delete_one(
///     &collection,
///     doc! { ... },
///     None::<DeleteOptions>,
///     None::<&mut ClientSession>,
/// ).await?;
/// ```
pub async fn delete_one<D>(
    collection: &Collection<D>,
    query: Document,
    options: Option<DeleteOptions>,
    session: Option<&mut ClientSession>,
) -> Result<DeleteResult, Error>
where
    D: Serialize + for<'a> Deserialize<'a>,
{
    let result = if let Some(session) = session {
        collection
            .delete_one_with_session(query, options, session)
            .await
    } else {
        collection.delete_one(query, options).await
    }
    .map_err(|err| Error::Mongo(err))?;

    Ok(result)
}

/// Executes a `deleteMany` query on `Collection`
///
/// https://www.mongodb.com/docs/manual/reference/method/db.collection.deleteMany/
///
/// ---
/// Example Usage:
/// ```
///
/// let collection: Collection<D> = ...;
///
/// let deletion_result: DeleteResult = delete_many(
///     &collection,
///     doc! { ... },
///     None::<DeleteOptions>,
///     None::<&mut ClientSession>,
/// ).await?;
/// ```
pub async fn delete_many<D>(
    collection: &Collection<D>,
    query: Document,
    options: Option<DeleteOptions>,
    session: Option<&mut ClientSession>,
) -> Result<DeleteResult, Error>
where
    D: Serialize + for<'a> Deserialize<'a>,
{
    let result = if let Some(session) = session {
        collection
            .delete_many_with_session(query, options, session)
            .await
    } else {
        collection.delete_many(query, options).await
    }
    .map_err(|err| Error::Mongo(err))?;

    Ok(result)
}
