// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use crate::error::Error;
use mongodb::{
    bson::Document,
    options::{FindOneAndUpdateOptions, UpdateOptions},
    results::UpdateResult,
    ClientSession, Collection,
};
use serde::{Deserialize, Serialize};

/// Executes a `updateOne` query on `Collection`.
///
/// https://www.mongodb.com/docs/manual/reference/method/db.collection.updateOne/
///
/// ---
/// Example Usage:
/// ```
///
/// let collection: Collection<D> = ...;
///
/// let update_result: UpdateResult = update_one(
///     &collection,
///     doc! { ... },
///     doc! { ... },
///     None::<UpdateOptions>,
///     None::<&mut ClientSession>,
/// ).await?;
/// ```
pub async fn update_one<D>(
    collection: &Collection<D>,
    query: Document,
    update: Document,
    options: Option<UpdateOptions>,
    session: Option<&mut ClientSession>,
) -> Result<UpdateResult, Error>
where
    D: Serialize + for<'a> Deserialize<'a>,
{
    let result = if let Some(session) = session {
        collection
            .update_one_with_session(query, update, options, session)
            .await
    } else {
        collection.update_one(query, update, options).await
    }
    .map_err(|err| Error::Mongo(err))?;

    Ok(result)
}

/// Executes a `updateMany` query on `Collection`.
///
/// https://www.mongodb.com/docs/manual/reference/method/db.collection.updateMany/
///
/// ---
/// Example Usage:
/// ```
///
/// let collection: Collection<D> = ...;
///
/// let update_result: UpdateResult = update_many(
///     &collection,
///     doc! { ... },
///     doc! { ... },
///     None::<UpdateOptions>,
///     None::<&mut ClientSession>,
/// ).await?;
/// ```
pub async fn update_many<D>(
    collection: &Collection<D>,
    query: Document,
    update: Document,
    options: Option<UpdateOptions>,
    session: Option<&mut ClientSession>,
) -> Result<UpdateResult, Error>
where
    D: Serialize + for<'a> Deserialize<'a>,
{
    let result = if let Some(session) = session {
        collection
            .update_many_with_session(query, update, options, session)
            .await
    } else {
        collection.update_many(query, update, options).await
    }
    .map_err(|err| Error::Mongo(err))?;

    Ok(result)
}

/// Executes a `findOneAndUpdate` query on `Collection`,
/// returns the original document by default.
///
/// https://www.mongodb.com/docs/manual/reference/method/db.collection.findOneAndUpdate/
///
/// ---
/// Example Usage:
/// ```
///
/// let collection: Collection<D> = ...;
///
/// let original_document: D = find_one_and_update(
///     &collection,
///     doc! { ... },
///     doc! { ... },
///     None::<FindOneAndUpdateOptions>,
///     None::<&mut ClientSession>,
/// ).await?;
/// ```
pub async fn find_one_and_update<D>(
    collection: &Collection<D>,
    query: Document,
    update: Document,
    options: Option<FindOneAndUpdateOptions>,
    session: Option<&mut ClientSession>,
) -> Result<Option<D>, Error>
where
    D: Serialize + for<'a> Deserialize<'a>,
{
    let result = if let Some(session) = session {
        collection
            .find_one_and_update_with_session(query, update, options, session)
            .await
    } else {
        collection.find_one_and_update(query, update, options).await
    }
    .map_err(|err| Error::Mongo(err))?;

    Ok(result)
}
