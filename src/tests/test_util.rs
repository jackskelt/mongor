// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

use crate::{model::Model, test_db::TestDB};
use mongodb::{bson::oid::ObjectId, options::ClientOptions, Client};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Shark {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    pub oid: Option<ObjectId>,
    pub name: String,
    pub species: String,
    pub sightings: usize,
}

impl Shark {
    pub fn new(name: &str, species: &str) -> Self {
        Self {
            oid: None,
            name: name.to_string(),
            species: species.to_string(),
            sightings: 0,
        }
    }
}

pub async fn setup() -> (Client, TestDB, Model<Shark>) {
    let client_options =
        ClientOptions::parse("mongodb://localhost:27017,localhost:27018,localhost:27019/")
            .await
            .unwrap();

    let client = Client::with_options(client_options).unwrap();

    let test_db = TestDB::new(&client);

    let collection = test_db.db.collection("sharks");

    let model = Model::from(collection);

    (client, test_db, model)
}
