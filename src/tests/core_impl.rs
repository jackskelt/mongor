// Authors: Robert Lopez
// License: MIT (See `LICENSE.md`)

#[cfg(test)]
mod tests {
    use crate::{
        core::session::*,
        test_db::{assert_document::*, assert_model::*, test_error::*},
        test_error,
        tests::test_util::*,
    };
    use mongodb::{bson::doc, options::*};

    #[tokio::test]
    async fn test_insert_one() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    let oid = shark_model.insert_one(shark, None, None).await?;

                    AssertModel::from(&shark_model)
                        .assert_exists_by_oid(&oid, None)
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_insert_one_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    let oid = shark_model
                        .insert_one(shark, None, Some(&mut session))
                        .await?;

                    commit_transaction(&mut session).await?;

                    AssertModel::from(&shark_model)
                        .assert_exists_by_oid(&oid, None)
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_one() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    let oid = shark_model.insert_one(shark, None, None).await?;

                    let document_option = shark_model
                        .find_one(doc! { "_id": oid }, None, None)
                        .await?;

                    assert_document_is_some(&document_option)?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_one_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    let oid = shark_model
                        .insert_one(shark, None, Some(&mut session))
                        .await?;

                    let document_option = shark_model
                        .find_one(doc! { "_id": oid }, None, Some(&mut session))
                        .await?;

                    commit_transaction(&mut session).await?;

                    assert_document_is_some(&document_option)?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_update_one() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    let oid = shark_model.insert_one(shark, None, None).await?;

                    shark_model
                        .update_one(
                            doc! { "_id": oid },
                            doc! { "$set": { "name": "The Best Whale Shark" } },
                            None,
                            None,
                        )
                        .await?;

                    AssertModel::from(&shark_model)
                        .assert_exists_by_field_value("name", "The Best Whale Shark", None)
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_update_one_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    let oid = shark_model
                        .insert_one(shark, None, Some(&mut session))
                        .await?;

                    shark_model
                        .update_one(
                            doc! { "_id": oid },
                            doc! { "$set": { "name": "The Best Whale Shark" } },
                            None,
                            Some(&mut session),
                        )
                        .await?;

                    commit_transaction(&mut session).await?;

                    AssertModel::from(&shark_model)
                        .assert_exists_by_field_value("name", "The Best Whale Shark", None)
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_delete_one() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    shark_model.insert_one(shark, None, None).await?;

                    shark_model
                        .delete_one(doc! { "name": "Whale Shark" }, None, None)
                        .await?;

                    AssertModel::from(&shark_model)
                        .assert_count(doc! {}, 0, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_delete_one_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let shark = Shark::new("Whale Shark", "Rhincodon typus");

                    shark_model
                        .insert_one(shark, None, Some(&mut session))
                        .await?;

                    shark_model
                        .delete_one(doc! { "name": "Whale Shark" }, None, Some(&mut session))
                        .await?;

                    commit_transaction(&mut session).await?;

                    AssertModel::from(&shark_model)
                        .assert_count(doc! {}, 0, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_insert_many() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    let oids = shark_model.insert_many(sharks, None, None).await?;

                    AssertModel::from(&shark_model)
                        .assert_exists_by_oid(&oids[0], None)
                        .await?
                        .assert_exists_by_oid(&oids[1], None)
                        .await?
                        .assert_count(doc! {}, 2, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_insert_many_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    let oids = shark_model
                        .insert_many(sharks, None, Some(&mut session))
                        .await?;

                    commit_transaction(&mut session).await?;

                    AssertModel::from(&shark_model)
                        .assert_exists_by_oid(&oids[0], None)
                        .await?
                        .assert_exists_by_oid(&oids[1], None)
                        .await?
                        .assert_count(doc! {}, 2, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_many() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model.insert_many(sharks, None, None).await?;

                    let options = FindOptions::builder().sort(doc! { "name": -1 }).build();
                    let mut cursor = shark_model.find_many(doc! {}, Some(options), None).await?;

                    let whale_shark_option = cursor.next().await?;
                    let great_white_option = cursor.next().await?;

                    assert_document_is_some(&whale_shark_option)?;
                    assert_document_is_some(&great_white_option)?;

                    let whale_shark = whale_shark_option.unwrap();
                    let great_white = great_white_option.unwrap();

                    if whale_shark.name != "Whale Shark" || great_white.name != "Great White" {
                        test_error!("Find many sort option did not work");
                    }

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_many_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model
                        .insert_many(sharks, None, Some(&mut session))
                        .await?;

                    commit_transaction(&mut session).await?;

                    let options = FindOptions::builder().sort(doc! { "name": -1 }).build();
                    let mut cursor = shark_model
                        .find_many(doc! {}, Some(options), Some(&mut session))
                        .await?;

                    let whale_shark_option = cursor.next_with_session(&mut session).await?;
                    let great_white_option = cursor.next_with_session(&mut session).await?;

                    assert_document_is_some(&whale_shark_option)?;
                    assert_document_is_some(&great_white_option)?;

                    let whale_shark = whale_shark_option.unwrap();
                    let great_white = great_white_option.unwrap();

                    if whale_shark.name != "Whale Shark" || great_white.name != "Great White" {
                        test_error!("Find many sort option did not work");
                    }

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_update_many() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model.insert_many(sharks, None, None).await?;

                    shark_model
                        .update_many(doc! {}, doc! { "$set": { "name": "shark" } }, None, None)
                        .await?;

                    AssertModel::from(&shark_model)
                        .assert_count(doc! { "name": "shark" }, 2, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_update_many_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model
                        .insert_many(sharks, None, Some(&mut session))
                        .await?;

                    shark_model
                        .update_many(
                            doc! {},
                            doc! { "$set": { "name": "shark" } },
                            None,
                            Some(&mut session),
                        )
                        .await?;

                    commit_transaction(&mut session).await?;

                    AssertModel::from(&shark_model)
                        .assert_count(doc! { "name": "shark" }, 2, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_delete_many() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model.insert_many(sharks, None, None).await?;

                    shark_model
                        .delete_many(doc! { "sightings": 0 }, None, None)
                        .await?;

                    AssertModel::from(&shark_model)
                        .assert_count(doc! {}, 0, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_delete_many_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model
                        .insert_many(sharks, None, Some(&mut session))
                        .await?;

                    shark_model
                        .delete_many(doc! { "sightings": 0 }, None, Some(&mut session))
                        .await?;

                    commit_transaction(&mut session).await?;

                    AssertModel::from(&shark_model)
                        .assert_count(doc! {}, 0, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_one_and_update() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    shark_model.insert_one(shark, None, None).await?;

                    let original_document = shark_model
                        .find_one_and_update(
                            doc! { "name": "Whale Shark" },
                            doc! { "$set": { "name": "shark" }},
                            None,
                            None,
                        )
                        .await?;

                    assert_document_is_some(&original_document)?;

                    let original_document = original_document.unwrap();

                    if original_document.name != "Whale Shark" {
                        test_error!("find_one_and_update returned updated document by default");
                    }

                    AssertModel::from(&shark_model)
                        .assert_exists_by_field_value("name", "shark", None)
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_one_and_update_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    shark_model.insert_one(shark, None, Some(&mut session)).await?;

                    let options = FindOneAndUpdateOptions::builder()
                        .return_document(Some(ReturnDocument::After))
                        .build();

                    let updated_document = shark_model
                        .find_one_and_update(
                            doc! { "name": "Whale Shark" },
                            doc! { "$set": { "name": "shark" }},
                            Some(options),
                            Some(&mut session),
                        )
                        .await?;

                    commit_transaction(&mut session).await?;

                    assert_document_is_some(&updated_document)?;

                    let updated_document = updated_document.unwrap();

                    if updated_document.name != "shark" {
                        test_error!("find_one_and_update returned default document with ReturnDocument::After option provided");
                    }

                    AssertModel::from(&shark_model)
                        .assert_exists_by_field_value("name", "shark", None)
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_one_and_replace() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    shark_model.insert_one(shark, None, None).await?;

                    let replacement = Shark::new("Great White", "Carcharodon carcharias");

                    let original_document = shark_model
                        .find_one_and_replace(
                            doc! { "name": "Whale Shark" },
                            replacement,
                            None,
                            None,
                        )
                        .await?;

                    assert_document_is_some(&original_document)?;

                    let original_document = original_document.unwrap();

                    if original_document.name != "Whale Shark" {
                        test_error!("find_one_and_update returned updated document by default");
                    }

                    AssertModel::from(&shark_model)
                        .assert_exists(
                            doc! {
                                "name": "Great White",
                                "species": "Carcharodon carcharias",
                            },
                            None,
                        )
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_find_one_and_replace_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let shark = Shark::new("Whale Shark", "Rhincodon typus");
                    shark_model.insert_one(shark, None, Some(&mut session)).await?;

                    let replacement = Shark::new("Great White", "Carcharodon carcharias");

                    let options = FindOneAndReplaceOptions::builder()
                        .return_document(Some(ReturnDocument::After))
                        .build();

                    let updated_document = shark_model
                        .find_one_and_replace(
                            doc! { "name": "Whale Shark" },
                            replacement,
                            Some(options),
                            Some(&mut session),
                        )
                        .await?;

                    commit_transaction(&mut session).await?;

                    assert_document_is_some(&updated_document)?;

                    let updated_document = updated_document.unwrap();

                    if updated_document.name != "Great White" {
                        test_error!("find_one_and_replace returned default document with ReturnDocument::After option provided");
                    }

                    AssertModel::from(&shark_model)
                        .assert_exists(
                            doc! {
                                "name": "Great White",
                                "species": "Carcharodon carcharias",
                            },
                            None,
                        )
                        .await?
                        .assert_count(doc! {}, 1, None)
                        .await?;

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_aggregate() {
        let (_, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model.insert_many(sharks, None, None).await?;

                    let mut cursor = shark_model
                        .aggregate(
                            vec![
                                doc! {
                                    "$match": {},
                                },
                                doc! {
                                    "$addFields": {
                                        "nameAndSpecies": {
                                            "$concat": ["$name", " ", "$species"],
                                        },
                                    },
                                },
                                doc! {
                                    "$sort": {
                                        "nameAndSpecies": -1,
                                    },
                                },
                            ],
                            None,
                            None,
                        )
                        .await?;

                    let whale_shark_generic_option = cursor.next_generic().await?;
                    let great_white_generic_option = cursor.next_generic().await?;

                    assert_document_is_some(&whale_shark_generic_option)?;
                    assert_document_is_some(&great_white_generic_option)?;

                    let whale_shark_generic = whale_shark_generic_option.unwrap();
                    let great_white_generic = great_white_generic_option.unwrap();

                    if !whale_shark_generic.contains_key("nameAndSpecies") || !great_white_generic.contains_key("nameAndSpecies") {
                        test_error!("Aggregate did not apply $addFields operation");
                    }

                    let whale_shark_name_and_species = whale_shark_generic
                        .get("nameAndSpecies")
                        .unwrap()
                        .as_str()
                        .ok_or(TestError::from("nameAndSpecies field could not be converted to &str"))?;

                    let great_white_name_and_species = great_white_generic
                        .get("nameAndSpecies")
                        .unwrap()
                        .as_str()
                        .ok_or(TestError::from("nameAndSpecies field could not be converted to &str"))?;


                    if whale_shark_name_and_species != "Whale Shark Rhincodon typus" || great_white_name_and_species != "Great White Carcharodon carcharias" {
                        test_error!("Aggregate did not apply $addFields correctly, or did apply $sort correctly");
                    }

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_aggregate_with_session() {
        let (client, test_db, shark_model) = setup().await;

        test_db
            .run_test(
                || async {
                    let mut session = start_session(&client, None).await?;

                    start_transaction(&mut session, None).await?;

                    let sharks = vec![
                        Shark::new("Whale Shark", "Rhincodon typus"),
                        Shark::new("Great White", "Carcharodon carcharias"),
                    ];

                    shark_model.insert_many(sharks, None, Some(&mut session)).await?;

                    commit_transaction(&mut session).await?;

                    let mut cursor = shark_model
                        .aggregate(
                            vec![
                                doc! {
                                    "$match": {},
                                },
                                doc! {
                                    "$addFields": {
                                        "nameAndSpecies": {
                                            "$concat": ["$name", " ", "$species"],
                                        },
                                    },
                                },
                                doc! {
                                    "$sort": {
                                        "nameAndSpecies": -1,
                                    },
                                },
                            ],
                            None,
                            Some(&mut session),
                        )
                        .await?;

                    let whale_shark_generic_option = cursor.next_generic_with_session(&mut session).await?;
                    let great_white_generic_option = cursor.next_generic_with_session(&mut session).await?;

                    assert_document_is_some(&whale_shark_generic_option)?;
                    assert_document_is_some(&great_white_generic_option)?;

                    let whale_shark_generic = whale_shark_generic_option.unwrap();
                    let great_white_generic = great_white_generic_option.unwrap();

                    if !whale_shark_generic.contains_key("nameAndSpecies") || !great_white_generic.contains_key("nameAndSpecies") {
                        test_error!("Aggregate did not apply $addFields operation");
                    }

                    let whale_shark_name_and_species = whale_shark_generic
                        .get("nameAndSpecies")
                        .unwrap()
                        .as_str()
                        .ok_or(TestError::from("nameAndSpecies field could not be converted to &str"))?;

                    let great_white_name_and_species = great_white_generic
                        .get("nameAndSpecies")
                        .unwrap()
                        .as_str()
                        .ok_or(TestError::from("nameAndSpecies field could not be converted to &str"))?;


                    if whale_shark_name_and_species != "Whale Shark Rhincodon typus" || great_white_name_and_species != "Great White Carcharodon carcharias" {
                        test_error!("Aggregate did not apply $addFields correctly, or did apply $sort correctly");
                    }

                    Ok(())
                },
                None,
            )
            .await
            .unwrap();
    }
}
